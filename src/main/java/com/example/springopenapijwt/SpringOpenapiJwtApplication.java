package com.example.springopenapijwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOpenapiJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringOpenapiJwtApplication.class, args);
	}

}
