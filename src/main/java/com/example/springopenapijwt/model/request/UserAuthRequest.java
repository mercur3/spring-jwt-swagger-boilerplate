package com.example.springopenapijwt.model.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Getter
@Setter
public class UserAuthRequest {
	@Email
	private String email;

	@Size(min = 3)
	private String password;
}
