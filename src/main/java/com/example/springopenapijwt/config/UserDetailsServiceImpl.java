package com.example.springopenapijwt.config;

import com.example.springopenapijwt.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class UserDetailsServiceImpl implements UserDetailsService {
	private final UserRepository repo;

	@Autowired
	public UserDetailsServiceImpl(UserRepository repo) {
		this.repo = repo;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		var user = repo
				.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException(username));

		return new User(
				user.getEmail(),
				user.getPassword(),
				List.of(new SimpleGrantedAuthority(user.getRole().getName()))
		);
	}
}
