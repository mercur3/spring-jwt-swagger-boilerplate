package com.example.springopenapijwt.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

class JwtFilter extends OncePerRequestFilter {
	private final UserDetailsService userDetailsService;

	public JwtFilter(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Override
	public void doFilterInternal(
			HttpServletRequest request,
			HttpServletResponse response,
			FilterChain filterChain
	)
	throws
			ServletException,
			IOException
	{
		var header = request.getHeader(Jwt.JWT_HEADER);
		if (header != null && header.startsWith(Jwt.JWT_PREFIX)) {
			applyTokenFilter(response, header);
		}

		filterChain.doFilter(request, response);
	}

	private void applyTokenFilter(HttpServletResponse response, String header) throws IOException {
		var token = header.substring(Jwt.JWT_PREFIX.length());
		var opt = Jwt.decode(token);
		if (opt.isEmpty()) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid token");
			return;
		}

		var claim = opt.get().getClaim("email");
		if (claim == null) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Email not found");
			return;
		}
		try {
			var user = userDetailsService.loadUserByUsername(claim.asString());
			var auth = new UsernamePasswordAuthenticationToken(
					user.getUsername(),
					user.getPassword(),
					user.getAuthorities()
			);
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
		catch (UsernameNotFoundException ex) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "User not found");
		}
	}
}
