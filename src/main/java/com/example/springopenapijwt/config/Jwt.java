package com.example.springopenapijwt.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Optional;

@Slf4j
public class Jwt {
	private Jwt() {}

	/// FIXME should be hidden
	private static final String JWT_SECRET = "password";
	private static final Algorithm ALGORITHM = Algorithm.HMAC256(JWT_SECRET);

	public static final String JWT_PREFIX = "Bearer ";
	public static final long TOKEN_EXPIRATION_TIME = 24 * 3600 * 30 * 1000L; // one month
	public static final String JWT_HEADER = "Authorization";
	public static final String JWT_ISSUER = "auth0";

	public static String generateToken(String email, String role) {
		var now = System.currentTimeMillis();
		var expiration = now + TOKEN_EXPIRATION_TIME;

		return JWT
				.create()
				.withSubject("user details")
				.withClaim("email", email)
				.withClaim("role", role)
				.withIssuedAt(new Date(now))
				.withExpiresAt(new Date(expiration))
				.withIssuer(JWT_ISSUER)
				.sign(ALGORITHM);
	}

	public static Optional<DecodedJWT> decode(String token) {
		DecodedJWT decodedJWT = null;
		try {
			decodedJWT = JWT.require(ALGORITHM).withIssuer(JWT_ISSUER).build().verify(token);
		}
		catch (JWTVerificationException ex) {
			log.warn("Invalid token: " + token);
		}

		return Optional.ofNullable(decodedJWT);
	}
}
