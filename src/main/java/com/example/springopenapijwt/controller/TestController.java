package com.example.springopenapijwt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/test")
public class TestController {
	@GetMapping("/hello")
	public String hello(Principal principal) {
		return "Hello " + principal.getName();
	}
}
