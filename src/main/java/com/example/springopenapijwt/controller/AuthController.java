package com.example.springopenapijwt.controller;

import com.example.springopenapijwt.config.Jwt;
import com.example.springopenapijwt.config.SecurityConfig;
import com.example.springopenapijwt.model.User;
import com.example.springopenapijwt.model.request.UserAuthRequest;
import com.example.springopenapijwt.repo.RoleRepository;
import com.example.springopenapijwt.repo.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {
	private final UserRepository userRepo;
	private final RoleRepository roleRepo;
	private final PasswordEncoder passwordEncoder;
	private final AuthenticationManager authManager;

	@Autowired
	public AuthController(
			UserRepository userRepo,
			AuthenticationManager authManager,
			RoleRepository roleRepo
	) {
		this.userRepo = userRepo;
		this.passwordEncoder = SecurityConfig.PASSWORD_ENCODER;
		this.authManager = authManager;
		this.roleRepo = roleRepo;
	}

	@PostMapping("/login")
	public Map<String, ?> login(@RequestBody UserAuthRequest req) {
		var email = req.getEmail();
		var inputToken = new UsernamePasswordAuthenticationToken(email, req.getPassword());
		try {
			authManager.authenticate(inputToken);
		}
		catch (AuthenticationException ex) {
			var msg = "Login failed for user: " + email;
			log.warn(msg);
			return Map.of("error", msg);
		}

		return Map.of("jwt-token", Jwt.generateToken(email, "ROLE_USER"));
	}

	@PostMapping("/register")
	public Map<String, ?> register(
			@RequestBody UserAuthRequest req,
			BindingResult bindingResult
	) {
		if (bindingResult.hasErrors()) {
			return Map.of("error", bindingResult.getAllErrors());
		}

		var email = req.getEmail();
		var user = new User();
		user.setEmail(email);
		user.setPassword(passwordEncoder.encode(req.getPassword()));
		user.setRole(roleRepo.findByName("ROLE_USER").get());
		userRepo.save(user);

		return Map.of("jwt-token", Jwt.generateToken(email, "ROLE_USER"));
	}
}
